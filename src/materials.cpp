#include "Path/materials.h"
#include "Path/Ray.h"
#include <glm/geometric.hpp>
#define _USE_MATH_DEFINES
#include <cmath>

/**
 Calculate an orthonormal basis with Frisvad's method.

 @param e3 The z axis of the right hand system.
 @param e1 The x axis of the right hand system.
 @param e2 The y axis of the right hand system.
 */
void orthonormalBasis(const glm::vec3 &e3, glm::vec3 &e1, glm::vec3 &e2) {
	if (e3.z < -0.9999999f) { // Todo: Make this nicer (fp math)
		e1 = glm::vec3(0, -1, 0);
		e2 = glm::vec3(-1, 0, 0);
	} else {
		const float a = 1.0f / (1.0f + e3.z);
		const float b = -e3.x * e3.y * a;
		e1 = glm::vec3(1 - e3.x * e3.x*a, b, -e3.x);
		e2 = glm::vec3(b, 1 - e3.y * e3.y*a, -e3.y);
	}
}

UniformBRDF::UniformBRDF(float roughness) : roughness(roughness) {}

float UniformBRDF::operator()(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const {
    return roughness / (float)M_PI;
}

float UniformBRDF::pdf(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const {
    return 1.0f / (float)M_PI;
}

glm::vec3 UniformBRDF::sample(glm::vec3 normal, float uniform1, float uniform2) const {
    glm::vec3 e1;
    glm::vec3 e2;
    orthonormalBasis(normal, e1, e2);

    float phi = 2.0f * (float)M_PI * uniform1;
    float costheta = std::sqrt(uniform2);
    float sintheta = std::sqrt(1.0f - uniform2);

    return std::cos(phi) * sintheta * e1 + std::sin(phi) * sintheta * e2 + costheta * normal;
}

float GGXBRDF::operator()(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const {
    return 0.0f;
}

float GGXBRDF::pdf(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const {
    return 0.0f;
}

glm::vec3 GGXBRDF::sample(glm::vec3 normal, float uniform1, float uniform2) const {
    glm::vec3 e1, e2;
	orthonormalBasis(normal, e1, e2);

	float phi = 2 * M_PI * uniform1;
	float theta = std::atan(roughness * std::sqrt(uniform2) / std::sqrt(1 - uniform2));
	return std::cos(phi) * std::sin(theta) * e1 + std::sin(phi) * std::sin(theta) * e2 + std::cos(theta) * normal;
}

/**
 Defines the chi function.

 @param v: Argument of the chi function.
 @returns 1 if v is strictly greater than 0, 0 otherwise.
*/
float GGXBRDF::chi(float v) const {
    return v > 0.0f ? 1 : 0;
}

/**
 Defines the geometry distribution function of the microfacets in the GGX model.
 The geometry distribution function is a part of the BRDF in the Cook-Torrance microfacet
 model. It describes the occlusion of the facets by other facets.

 @param view View vector. The direction tracing back to the camera. This direction will
		most commonly be the negative of the ray direction (due to the way rays are traced).
 @param light Incoming light vector. The direction tracing back to the light source.
 @param halfVector Half vector. The vector half-way in between the view vector and incoming
		light vector. This is the facet normal in the microfacet model.
 @param n Surface normal to the collision point.
 @param alpha: Roughness factor.
 @returns Geometry distribution function.
*/
float GGXBRDF::geometry(glm::vec3 light, glm::vec3 view, glm::vec3 halfVector,
        glm::vec3 normal, float alpha) const {
    return geometryp(view, halfVector, normal, alpha) *
           geometryp(light, halfVector, normal, alpha);
}

/**
 Partial geometry distribution function. Describe "half" the geometry distribution function in
 the GGX model.

 @param omega Incoming light vector or view vector.
 @param halfVector Half vector. The vector half-way in between the view vector and incoming
		light vector. This is the facet normal in the microfacet model.
 @param normal Surface normal to the collision point.
 @param alpha Roughness factor.
 @returns Partial geometry distribution function.
*/
float GGXBRDF::geometryp(glm::vec3 omega, glm::vec3 halfVector, glm::vec3 normal,
        float alpha) const {
    float costheta = glm::dot(omega, halfVector);
	return chi(costheta / glm::dot(omega, normal)) * 2 /
           (1 + std::sqrt(1 + alpha * alpha * ((1 - costheta * costheta) / (costheta * costheta))));
}

/**
 Schlick fresnel approximation. Approximates the fresnel distribution effectively.

 @param ior1 Index of refraction of incoming medium.
 @param ior2 Index of refraction of outgoing medium.
 @param light Incoming light vector. The direction tracing back to the light source.
 @param halfVector Half vector. The vector half-way in between the view vector and incoming
		light vector. This is the facet normal in the microfacet model.
 @returns The fresnel coefficient of the incident angle.
*/
float GGXBRDF::schlickFresnel(float ior1, float ior2, glm::vec3 light,
        glm::vec3 halfVector) const {
    float f0 = (ior1 - ior2) / (ior1 + ior2);
	f0 = f0 * f0;
	return f0 + (1 - f0)*std::pow(1 - glm::dot(light, halfVector), 5);
}

Emission::Emission(glm::vec3 color, float brightness) : color(color), brightness(brightness) {}

glm::vec3 Emission::eval(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const {
    return color * brightness;
}

float Emission::pdf(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const {
    return 0.0f;
}

glm::vec3 Emission::sample(glm::vec3 normal, float uniform1, float uniform2) const {
    return glm::vec3();
}

LambertianDiffuse::LambertianDiffuse(glm::vec3 color, float roughness) : color(color), brdf(roughness) {}

glm::vec3 LambertianDiffuse::eval(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const {
    return color * brdf(light, view, normal);
}

float LambertianDiffuse::pdf(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const {
    return brdf.pdf(light, view, normal);
}

glm::vec3 LambertianDiffuse::sample(glm::vec3 normal, float uniform1, float uniform2) const {
    return brdf.sample(normal, uniform1, uniform2);
}
