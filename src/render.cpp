#include "Path/render.h"
#include "Path/Ray.h"
#include "Path/materials.h"
#include <glm/geometric.hpp>
#include <vector>
#include <cmath>
#include <stdexcept>

#define MAX_BOUNCES 64
#define EPS 0.000001f
#define RAY_MIN_DIST 0.0001f

Renderer::Renderer(std::vector<std::shared_ptr<Primitive>> objects,
				   std::vector<std::shared_ptr<Primitive>> lights,
				   std::unique_ptr<Camera> camera) :
	objects(objects), lights(lights), camera(std::move(camera)), eng(), dist(0, 1) {
	image = std::vector<float>(this->camera->dimX * this->camera->dimY * 3, 0);
	spp = std::vector<unsigned int>(this->camera->dimX * this->camera->dimY, 0);
}

glm::vec3 Renderer::radiance(Ray &r) {
	float t;
	glm::vec3 color = glm::vec3();
	glm::vec3 throughput = glm::vec3(1.0f);
	for (int i = 0; i < MAX_BOUNCES; ++i) {
		std::shared_ptr<Primitive> obj = nearestIntersection(r, t);

		// If we hit the light source or missed everything end the loop
		// We end on light source hit since we don't want to "double dip"
		// with direct illumination and hitting the light source.
		if (obj == nullptr)
			break;

		if(obj == lights[0]) {
			// If this was the primary ray we still need to add the color
			if (r.depth() == 0)
				color = lights[0]->material->eval(glm::vec3(), glm::vec3(), glm::vec3());

			break;
		}

		glm::vec3 normal = obj->normal(r.collision());

		// Direct illumination
		glm::vec3 lightPos = lights[0]->sample(dist(eng), dist(eng));
		glm::vec3 shadowDir = glm::normalize(r.collision() - lightPos);
		Ray shadowRay(lightPos, shadowDir);
		float shadowDistance;
		if (obj == nearestIntersection(shadowRay, shadowDistance) && glm::length(shadowRay.collision() - r.collision()) < EPS) {
			// TODO: Fix bug with short distances
			color += std::abs(glm::dot(shadowDir, lights[0]->normal(lightPos)) /
				     (shadowDistance * shadowDistance) *
					 glm::dot(-shadowDir, normal)) /
					 lights[0]->pdf() *
				     throughput *
					 obj->material->eval(-shadowDir, -r.direction(), normal) *
					 lights[0]->material->eval(-shadowDir, -r.direction(), normal);
		}

		// Indirect illumination
		glm::vec3 light = obj->material->sample(normal, dist(eng), dist(eng));
		throughput *= (1.0f / obj->material->pdf(light, -r.direction(), normal)) * obj->material->eval(light, -r.direction(), normal);

		float p = std::min(1.0f, std::max(throughput.x, std::max(throughput.y, throughput.z)));
		if (dist(eng) > p)
			break;

		throughput *= 1 / p;

		r = Ray(r.collision(), light, r.depth() + 1);
	}

	return color;
}

void Renderer::render() {
	// Quick and dirty parallelization
	#pragma omp parallel for
	for (unsigned int i = 0; i < camera->dimX; ++i) {
		for (unsigned int j = 0; j < camera->dimY; ++j) {
			Ray r = camera->generatePrimaryRay(i, j, dist(eng), dist(eng));
			glm::vec3 pixelColor = radiance(r);

			image[3 * (i + j * camera->dimX)] *= spp[i + j * camera->dimX];
			image[3 * (i + j * camera->dimX) + 1] *= spp[i + j * camera->dimX];
			image[3 * (i + j * camera->dimX) + 2] *= spp[i + j * camera->dimX];

			image[3 * (i + j * camera->dimX)] += pixelColor.x;
			image[3 * (i + j * camera->dimX) + 1] += pixelColor.y;
			image[3 * (i + j * camera->dimX) + 2] += pixelColor.z;

			++spp[i + j * camera->dimX];

			image[3 * (i + j * camera->dimX)] /= spp[i + j * camera->dimX];
			image[3 * (i + j * camera->dimX) + 1] /= spp[i + j * camera->dimX];
			image[3 * (i + j * camera->dimX) + 2] /= spp[i + j * camera->dimX];
		}
	}
}

float *Renderer::data() {
	return &image[0];
}

std::shared_ptr<Primitive> Renderer::nearestIntersection(Ray &r, float &t) const {
	std::shared_ptr<Primitive> nearest;
	float minDist = std::numeric_limits<float>::max();
	for (std::shared_ptr<Primitive> obj : objects) {
		float objDist = obj->intersect(r);
		if (objDist > RAY_MIN_DIST && objDist < minDist) { //TODO: Make this nicer
			minDist = objDist;
			nearest = obj;
		}
	}

	if (nearest != nullptr) {
		t = minDist;
		r.setCollision(t * r.direction() + r.position());
	}

	return nearest;
}

// TODO: Fix bug with looking at 0
Camera::Camera(glm::vec3 position, glm::vec3 lookAt, glm::vec3 up,
		float focalLength, float pixelSize,
		unsigned int dimX, unsigned int dimY)
	: eye(-focalLength * lookAt + position), canvas(position),
	x(dimX * pixelSize * glm::normalize(glm::cross(lookAt, up))),
	y(dimY * pixelSize * glm::normalize(up)),
	aspect((float)dimX/(float)dimY),
	dimX(dimX), dimY(dimY) {}

Ray Camera::generatePrimaryRay(unsigned int i, unsigned int j,
	float u1, float u2) const {
	if (i >= dimX || j >= dimY) {
		throw std::out_of_range("Index out of range");
	}

	// Find boundaries of pixel in the coordinate system of the camera
	float xMax = (float((i + 1)) / float(dimX) - 1.0f / 2.0f);
	float xMin = (float(i) / float(dimX) - 1.0f / 2.0f);
	float yMax = (float((j + 1)) / float(dimY) - 1.0f / 2.0f) * aspect;
	float yMin = (float(j) / float(dimY) - 1.0f / 2.0f) * aspect;

	float xCoord = u1*(xMax - xMin) + xMin;
	float yCoord = u2*(yMax - yMin) + yMin;

	// Find midpoint
	//float xCoord = (xMax + xMin) / 2.0f;
	//float yCoord = (yMax + yMin) / 2.0f;

	glm::vec3 p = canvas + xCoord * x + yCoord * y;
	glm::vec3 v = glm::normalize(p - eye);
	return Ray(p, v, 0);
}

glm::vec3 Camera::getLookAt() const {
	return glm::normalize(glm::cross(x, y));
}
