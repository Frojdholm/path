#version 330 core
out vec4 fragColor;
in vec2 texCoord;
uniform sampler2D tex;

void main() {
   fragColor = pow(texture(tex, texCoord), vec4(vec3(1/2.2), 1));
}
