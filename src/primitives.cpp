#include "Path/primitives.h"
#include "Path/Ray.h"
#include <glm/geometric.hpp>
#define _USE_MATH_DEFINES
#include <cmath>
#include <stdexcept>

bool solveQuadratic(float p, float q, float &x1, float &x2) {
	p = p / 2.0f;
	float desc = p * p - q;
	if (desc < 0)
		return false;
	x1 = -p - sgn(p) * std::sqrt(desc);
	x2 = q / x1;
	if (x2 < x1)
		std::swap(x1, x2);
	return true;
}

/**
 Primitive constructor. Position and color are copied into the class.

 @param position Position of the Primitive.
 @param color Diffuse color of the Primitive.
 */
Primitive::Primitive(glm::vec3 position, Material *material) : position(position), material(material) {}

/**
 Sphere constructor. The radius squared is pre-computed to for intersection calculations.

 @param position Position of the Sphere.
 @param color Diffuse color of the Sphere.
 @param raduis Radius of the sphere.
 */
Sphere::Sphere(glm::vec3 position, Material *material, float radius) : Primitive(position, material), radius(radius) {
	R2 = radius * radius;
}

/**
 Calculates the intersection point of a Ray.

 @param r Ray to calculate intersection of.
 @returns The distance to the closest intersection of the ray or -1 if the ray
		does not intersect the Sphere.
 */
float Sphere::intersect(const Ray &r) const {
	glm::vec3 d = r.position() - position;
	float vd = glm::dot(d, r.direction());

	float d2 = glm::dot(d, d);

	float t1, t2;

	if (solveQuadratic(2*vd, d2 - R2, t1, t2)) {
		return t1 > 0.0f ? t1 : t2;
	}
	return -1.0f; // Negative distance means no collision
}

/**
 Calculate the normal of a point on the Sphere.

 @param position Position on the Sphere to calculate normal from.
 @returns The normal.
 */
glm::vec3 Sphere::normal(glm::vec3 position) const {
	glm::vec3 n = position - this->position; // TODO: Restrict to positions on the Sphere...
	return glm::normalize(n);
}

glm::vec3 Sphere::sample(float uniform1, float uniform2) const {
	return glm::vec3(); //IMPLEMENT
}

float Sphere::pdf() const {
	return 0.0f; //IMPLEMENT
}

Plane::Plane(glm::vec3 position, Material *material, glm::vec3 normal) : Primitive(position, material), n(glm::normalize(normal)) {}

float Plane::intersect(const Ray &r) const {
	float numerator = -glm::dot(n, r.position() - position);
	float denominator = glm::dot(n, r.direction());
	if (denominator == 0.0f)
		return -1.0f; // TODO: Take care of special case when num and den == 0

	return numerator / denominator;
}

glm::vec3 Plane::normal(glm::vec3 position) const {
	return n; // TODO: Restrict to positions on the plane..
}

glm::vec3 Plane::sample(float uniform1, float uniform2) const {
	return glm::vec3(); //IMPLEMENT
}

float Plane::pdf() const {
	return 0.0f; //IMPLEMENT
}

Triangle::Triangle(Material *material, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3) : Plane((p1 + p2 + p3)/3.0f, material, glm::cross(p2 - p1, p3 - p1)),
		p1(p1), p2(p2), p3(p3) {}

/*Möller-Trumbore algorithm for triangle intersection*/
float Triangle::intersect(const Ray &r) const {
	glm::vec3 T = r.position() - p1;
	glm::vec3 E1 = p2 - p1;
	glm::vec3 E2 = p3 - p1;
	glm::vec3 P = glm::cross(r.direction(), E2);
	glm::vec3 Q = glm::cross(T, E1);

	float den = glm::dot(P, E1);

	float t = glm::dot(Q, E2) / den;
	float u = glm::dot(P, T) / den;
	float v = glm::dot(Q, r.direction()) / den;

	if (u < 0.0f || v < 0.0f || u + v > 1.0f)
		return -1.0f;
	return t;
}

glm::vec3 Triangle::normal(glm::vec3 position) const {
	return n; // TODO: Restrict to positions on the triangle
}

glm::vec3 Triangle::sample(float uniform1, float uniform2) const {
	return glm::vec3(); //IMPLEMENT
}

float Triangle::pdf() const {
	return 0.0f; //IMPLEMENT
}

Rectangle::Rectangle(glm::vec3 position, Material *material, glm::vec3 e1, glm::vec3 e2, float width, float height) : Plane(position, material, glm::cross(e1, e2)) {
	e1 = glm::normalize(e1);
	e2 = glm::normalize(e2);

	p1 = position - width/2 * e1 - height/2 * e2;
	p2 = position + width/2 * e1 - height/2 * e2;
	p3 = position + width/2 * e1 + height/2 * e2;
	p4 = position - width/2 * e1 + height/2 * e2;
}

/*Modified Möller-Trumbore algorithm*/
float Rectangle::intersect(const Ray &r) const {
	glm::vec3 T = r.position() - p1;
	glm::vec3 E1 = p2 - p1;
	glm::vec3 E2 = p4 - p1;
	glm::vec3 P = glm::cross(r.direction(), E2);
	glm::vec3 Q = glm::cross(T, E1);

	float den = glm::dot(P, E1);

	float t = glm::dot(Q, E2) / den;
	float u = glm::dot(P, T) / den;
	float v = glm::dot(Q, r.direction()) / den;

	if (u < 0.0f || v < 0.0f || u > 1.0f || v > 1.0f)
		return -1.0f;
	return t;
}

glm::vec3 Rectangle::normal(glm::vec3 position) const {
	return n; // TODO: restrict to positions on the rectangle
}

glm::vec3 Rectangle::sample(float uniform1, float uniform2) const {
	glm::vec3 e21 = p2 - p1;
	glm::vec3 e41 = p4 - p1;
	return p1 + uniform1 * e21 + uniform2 * e41;
}

float Rectangle::pdf() const {
	return 1 / (glm::length(p4 - p1) * glm::length(p2 - p1));
}
