#pragma once

#include "Path/Ray.h"
#include <glm/vec3.hpp>
#include <memory>
#include <random>

void orthonormalBasis(const glm::vec3 &e3, glm::vec3 &e1, glm::vec3 &e2);

class BRDF {
    public:
    virtual float operator()(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const = 0;
    virtual float pdf(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const = 0;
    virtual glm::vec3 sample(glm::vec3 normal, float uniform1, float uniform2) const = 0;
};

class UniformBRDF : public BRDF {
    float roughness;

    public:
    UniformBRDF(float roughness);

    virtual float operator()(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const override;
    virtual float pdf(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const override;
    virtual glm::vec3 sample(glm::vec3 normal, float uniform1, float uniform2) const override;
};

class GGXBRDF : public BRDF {
    float roughness;
    float ior;

    float chi(float v) const;
	float geometry(glm::vec3 light, glm::vec3 view, glm::vec3 halfVector,
            glm::vec3 normal, float alpha) const;
	float geometryp(glm::vec3 omega, glm::vec3 halfVector, glm::vec3 normal,
            float alpha) const;
	float schlickFresnel(float ior1, float ior2, glm::vec3 incoming,
            glm::vec3 halfVector) const;

    public:
    GGXBRDF(float roughness, float ior);
    virtual float operator()(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const override;
    virtual float pdf(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const override;
    virtual glm::vec3 sample(glm::vec3 normal, float uniform1, float uniform2) const override;
};

class Material {
    public:
    virtual ~Material() = default;

    virtual glm::vec3 eval(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const = 0;
    virtual float pdf(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const = 0;
    virtual glm::vec3 sample(glm::vec3 normal, float uniform1, float uniform2) const = 0;
};

class Emission : public Material {
    glm::vec3 color;
    float brightness;

    public:
    Emission(glm::vec3 color, float brightness);

    virtual glm::vec3 eval(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const override;
    virtual float pdf(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const override;
    virtual glm::vec3 sample(glm::vec3 normal, float uniform1, float uniform2) const override;
};

class LambertianDiffuse : public Material {
    glm::vec3 color;

    UniformBRDF brdf;

    public:
    LambertianDiffuse(glm::vec3 color, float roughness);

    virtual glm::vec3 eval(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const override;
    virtual float pdf(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const override;
    virtual glm::vec3 sample(glm::vec3 normal, float uniform1, float uniform2) const override;
};

class SpecularGGX : public Material {
    glm::vec3 color;

    GGXBRDF brdf;

    public:
    SpecularGGX(glm::vec3 color, float roughness, float ior);

    virtual glm::vec3 eval(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const override;
    virtual float pdf(glm::vec3 light, glm::vec3 view, glm::vec3 normal) const override;
    virtual glm::vec3 sample(glm::vec3 normal, float uniform1, float uniform2) const override;
};
