#pragma once

#include "Path/primitives.h"
#include <glm/mat4x4.hpp>
#include <vector>

class Mesh {
    std::vector<Triangle> triangles;
    glm::mat4 model;

public:
    Mesh();
    void setModelMatrix(glm::mat4 matrix);
    void addTriangle(Triangle tri);
    void registerTriangles(std::vector<Primitive> &objects) const;
};
