#pragma once

#include "Path/Vec3.h"
#include "Path/primitives.h"
#include <glm/vec3.hpp>
#include <memory>
#include <vector>
#include <random>

class Ray;


class Camera{
private:
	glm::vec3 eye;
	glm::vec3 canvas;

	glm::vec3 x;
	glm::vec3 y;

	float aspect;
public:
	const unsigned int dimX;
	const unsigned int dimY;

	Camera(glm::vec3 position, glm::vec3 lookAt, glm::vec3 up,
		float focalLength, float pixelSize,
		unsigned int dimX, unsigned int dimY);
	Ray generatePrimaryRay(unsigned int i, unsigned int j, float u1, float u2) const;
	glm::vec3 getLookAt() const;
};

class Renderer {
private:
	std::vector<std::shared_ptr<Primitive>> objects;
	std::vector<std::shared_ptr<Primitive>> lights;
	std::unique_ptr<Camera> camera;
	std::vector<float> image;
	std::vector<unsigned int> spp;

	std::default_random_engine eng;
	std::uniform_real_distribution<float> dist;

	glm::vec3 radiance(Ray &r);

public:
	Renderer(std::vector<std::shared_ptr<Primitive>> objects, std::vector<std::shared_ptr<Primitive>> lights, std::unique_ptr<Camera> camera);
	void render();
	float *data();
	std::shared_ptr<Primitive> nearestIntersection(Ray &r, float &t) const;
};
