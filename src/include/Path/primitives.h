#pragma once

#include "Path/materials.h"
#include <glm/vec3.hpp>
#include <glm/mat3x4.hpp>
#include <memory>

class Ray;

bool solveQuadratic(float p, float q, float &x1, float &x2);

/*https://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c*/
template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}
/**
 * Primitive defines a geometrical primitive that can interact with rays.
 */
class Primitive {
protected:
	glm::vec3 position;

public:
	std::unique_ptr<Material> material;
	Primitive(glm::vec3 position, Material *material);
	virtual ~Primitive() = default;

	virtual glm::vec3 normal(glm::vec3 position) const = 0;
	virtual float intersect(const Ray &r) const = 0;
	virtual glm::vec3 sample(float uniform1, float uniform2) const = 0;
	virtual float pdf() const = 0;
};

/**
 * Defines a sphere.
 */
class Sphere : public Primitive {
	float radius;
	float R2;

public:
	Sphere(glm::vec3 position, Material *material, float radius);
	virtual float intersect(const Ray &r) const override;
	virtual glm::vec3 normal(glm::vec3 position) const override;
	virtual glm::vec3 sample(float uniform1, float uniform2) const override;
	virtual float pdf() const override;
};

class Plane : public Primitive {
protected:
	glm::vec3 n;

public:
	Plane(glm::vec3 position, Material *material, glm::vec3 normal);
	virtual float intersect(const Ray &r) const override;
	virtual glm::vec3 normal(glm::vec3 position) const override;
	virtual glm::vec3 sample(float uniform1, float uniform2) const override;
	virtual float pdf() const override;
};

class Triangle : public Plane {
	glm::vec3 p1, p2, p3;

public:
	Triangle(Material *material, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3);
	virtual float intersect(const Ray &r) const override;
	virtual glm::vec3 normal(glm::vec3 position) const override;
	virtual glm::vec3 sample(float uniform1, float uniform2) const override;
	virtual float pdf() const override;
};

class Rectangle : public Plane {
	glm::vec3 p1, p2, p3, p4;

public:
	Rectangle(glm::vec3 position, Material *material, glm::vec3 e1, glm::vec3 e2, float width, float height);
	virtual float intersect(const Ray &r) const override;
	virtual glm::vec3 normal(glm::vec3 position) const override;
	virtual glm::vec3 sample(float uniform1, float uniform2) const override;
	virtual float pdf() const override;
};
