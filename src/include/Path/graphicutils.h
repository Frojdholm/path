#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

class Shader {
	GLuint program;

public:
	Shader();
	void compile(const char *vertPath, const char *fragPath);
	void use() const;
	GLuint getUniformLocation(const char *uniform) const;
	void setInt(GLuint location, int attribute);
};

class Texture2D {
	GLuint texture;
	const unsigned int width;
	const unsigned int height;

public:
	Texture2D(unsigned int width, unsigned int height, float *data);
	Texture2D(const Texture2D &other) = delete;
	~Texture2D();
	Texture2D &operator=(const Texture2D &other) = delete;
	void bind() const;
	void unbind() const;
	void setIntParam(GLuint parameter, GLuint value);
	void subImage(float *data);
};