#pragma once

#include <glm/vec3.hpp>

class Ray {
	glm::vec3 _position;
	glm::vec3 _direction;
	int _depth;

	glm::vec3 _collision;

	public:
	Ray(glm::vec3 position = glm::vec3(), glm::vec3 direction = glm::vec3(), int depth = int(), glm::vec3 collision = glm::vec3());
	void setCollision(glm::vec3 collision);
	glm::vec3 position() const;
	glm::vec3 direction() const;
	int depth() const;
	glm::vec3 collision() const;
};
