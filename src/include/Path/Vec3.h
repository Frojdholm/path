#pragma once

#include <cmath>

template<typename T = float>
struct Vec3 {
	T x;
	T y;
	T z;

	Vec3() : x(T()), y(T()), z(T()) {}

	Vec3(T x, T y, T z) : x(x), y(y), z(z) {}

	inline Vec3 operator+(const Vec3 &v) const {
		return Vec3(x + v.x, y + v.y, z + v.z);
	}

	inline void operator+=(const Vec3 &v) {
		x += v.x;
		y += v.y;
		z += v.z;
	}

	inline Vec3 operator-(const Vec3 &v) const {
		return Vec3(x - v.x, y - v.y, z - v.z);
	}

	inline Vec3 operator-() const {
		return Vec3(-x, -y, -z);
	}

	inline void operator-=(const Vec3 &v) {
		x -= v.x;
		y -= v.y;
		z -= v.z;
	}

	inline T operator*(const Vec3 &v) const {
		return x * v.x + y * v.y + z * v.z;
	}

	inline Vec3 operator*(const T &t) const {
		return Vec3(x * t, y * t, z * t);
	}

	inline void operator*=(const T &t) {
		x *= t;
		y *= t;
		z *= t;
	}

	inline Vec3 operator^(const Vec3 &v) const {
		return Vec3(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
	}

	inline Vec3 elementwiseProduct(const Vec3 &v) const {
		return Vec3(x * v.x, y * v.y, z * v.z);
	}

	inline float magnitudeSq() const {
		return (*this)*(*this);
	}

	inline void normalize();

	inline void clamp(T value);

	/**
	 * Frisvad method of finding orthonormal basis.
	 */
	inline void orthonormal(Vec3 &e1, Vec3 &e2) const {
		if (z < -0.9999999f) { // Todo: Make this nicer (fp math)
			e1 = Vec3<>(0, -1, 0);
			e2 = Vec3<>(-1, 0, 0);
		} else {
			const float a = 1.0f / (1.0f + z);
			const float b = -x * y * a;
			e1 = Vec3<>(1 - x*x*a, b, -x);
			e2 = Vec3<>(b, 1 - y*y*a, -y);
		}
	}
};

template<>
inline void Vec3<float>::normalize() {
	float len = std::sqrt(magnitudeSq());
	x /= len;
	y /= len;
	z /= len;
}

template<>
inline void Vec3<float>::clamp(float value) {
	x = x > value ? value : x;
	y = y > value ? value : y;
	z = z > value ? value : z;
}

template<typename T = float>
inline Vec3<T> operator*(const T &t, const Vec3<T> &v) {
	return Vec3<T>(v.x * t, v.y * t, v.z * t);
}
