#include "Path/graphicutils.h"
#include <iostream>
#include <fstream>
#include <sstream>

#define SHADER_LOG_SIZE 512

Shader::Shader() {}

/**
 * Gets the shader sources and compiles them into a
 * program.
 */
void Shader::compile(const char *vertPath, const char *fragPath) {
	std::ifstream vertShader, fragShader;
	std::string vertSource, fragSource;

	// Enable exceptions so we can tell if the file reading failed
	vertShader.exceptions(std::ifstream::badbit | std::ifstream::failbit);
	fragShader.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	try {
		vertShader.open(vertPath);
		fragShader.open(fragPath);

		std::stringstream vertStream, fragStream;

		vertStream << vertShader.rdbuf();
		fragStream << fragShader.rdbuf();

		vertSource = vertStream.str();
		fragSource = fragStream.str();
	} catch (std::ifstream::failure e) {
		// TODO: Use something better than runtime error
		throw std::runtime_error("ERROR::SHADER failed to read source files");
	}

	const char *vertSourceCstr = vertSource.c_str();
	const char *fragSourceCstr = fragSource.c_str();

	// Compile the vertex shader
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertSourceCstr, NULL);
	glCompileShader(vertexShader);

	int success;
	char log[SHADER_LOG_SIZE];

	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, SHADER_LOG_SIZE, NULL, log);
		// TODO: Use something better than runtime error
		std::cerr << log << std::endl;
		throw std::runtime_error("ERROR::SHADER failed to compile vertex shader");
	}

	// Compile the fragment shader
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragSourceCstr, NULL);
	glCompileShader(fragmentShader);

	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(fragmentShader, SHADER_LOG_SIZE, NULL, log);
		// TODO: Use something better than runtime error
		std::cerr << log << std::endl;
		throw std::runtime_error("ERROR::SHADER failed to compile fragment shader");
	}

	// Link the program
	program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(program, SHADER_LOG_SIZE, NULL, log);
		std::cerr << log << std::endl;
		throw std::runtime_error("ERROR::SHADER failed to link program");
	}

	// Delete the shaders since they're not needed anymore
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

/**
 * Activates this shader
 */
void Shader::use() const {
	glUseProgram(program);
}
 /**
  * Returns the location of a uniform in the shader.
  *
  * @param uniform Name of the uniform in the shader source.
  */
GLuint Shader::getUniformLocation(const char *uniform) const {
	return glGetUniformLocation(program, uniform);
}
 /**
  * Sets an int uniform in the shader.
  *
  * @param location Location of the uniform in the shader source.
  * @param attribute Value to store in the uniform.
  */
void Shader::setInt(GLuint location, int attribute) {
	use(); // Make sure we're using this program.

	glUniform1i(location, attribute);
}

/**
 * Creates a Texture2D object on the GPU with the supplied data.
 *
 * @param width Width of the texture.
 * @param height Height of the texture.
 * @param data Data to store in the texture.
 */
Texture2D::Texture2D(unsigned int width, unsigned int height, float *data) :
	width(width), height(height) {
	glGenTextures(1, &texture);
	
	bind();
	// TODO: Check that data is correct size
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_FLOAT, data);
	unbind();
}

/**
 * Delete the texture from the GPU.
 */
Texture2D::~Texture2D() {
	glDeleteTextures(1, &texture);
}

/**
 * Bind this texture for use.
 */
void Texture2D::bind() const {
	glBindTexture(GL_TEXTURE_2D, texture);
}

/**
 * Unbind this texture.
 */
void Texture2D::unbind() const {
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture2D::setIntParam(GLuint parameter, GLuint value) {
	glTexParameteri(GL_TEXTURE_2D, parameter, value);
}

void Texture2D::subImage(float *data) {
	bind(); // Make sure we're using this texture
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGB, GL_FLOAT, data);
}