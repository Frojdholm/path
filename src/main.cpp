#include "Path/render.h"
#include "Path/primitives.h"
#include "Path/materials.h"
#include "Path/Ray.h"
#include "Path/graphicutils.h"
#include <glm/vec3.hpp>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <vector>
#include <memory>
#include <iostream>

static void error_callback(int error, const char* description) {
	std::cerr << description << std::endl;
}

int main(int argc, char* argv[]) {
	const unsigned int width = 300;
	const unsigned int height = 300;

	std::shared_ptr<Primitive> tri1(new Triangle(new LambertianDiffuse(glm::vec3(0.6f, 0.6f, 0.6f), 1.0f),
		glm::vec3(-0.2f, -1.0f, 0.5f), glm::vec3(-0.7f, -0.5f, 0.5f), glm::vec3(-0.2f, -0.5f, 0.5f)));
	std::shared_ptr<Primitive> tri2(new Triangle(new LambertianDiffuse(glm::vec3(0.6f, 0.6f, 0.6f), 1.0f),
		glm::vec3(-0.7f, -1.0f, 0.5f), glm::vec3(-0.7f, -0.5f, 0.5f), glm::vec3(-0.2f, -1.0f, 0.5f)));

	std::shared_ptr<Primitive> tri3(new Triangle(new LambertianDiffuse(glm::vec3(0.6f, 0.6f, 0.6f), 1.0f),
		glm::vec3(-0.7f, -0.5f, 1.0f), glm::vec3(-0.7f, -0.5f, 0.5f), glm::vec3(-0.7f, -1.0f, 1.0f)));
	std::shared_ptr<Primitive> tri4(new Triangle(new LambertianDiffuse(glm::vec3(0.6f, 0.6f, 0.6f), 1.0f),
		glm::vec3(-0.7f, -1.0f, 0.5f), glm::vec3(-0.7f, -1.0f, 1.0f), glm::vec3(-0.7f, -0.5f, 0.5f)));

	std::shared_ptr<Primitive> tri5(new Triangle(new LambertianDiffuse(glm::vec3(0.6f, 0.6f, 0.6f), 1.0f),
		glm::vec3(-0.2f, -0.5f, 0.5f), glm::vec3(-0.2f, -0.5f, 1.0f), glm::vec3(-0.2f, -1.0f, 1.0f)));
	std::shared_ptr<Primitive> tri6(new Triangle(new LambertianDiffuse(glm::vec3(0.6f, 0.6f, 0.6f), 1.0f),
		glm::vec3(-0.2f, -1.0f, 1.0f), glm::vec3(-0.2f, -1.0f, 0.5f), glm::vec3(-0.2f, -0.5f, 0.5f)));

	std::shared_ptr<Primitive> tri7(new Triangle(new LambertianDiffuse(glm::vec3(0.6f, 0.6f, 0.6f), 1.0f),
		glm::vec3(-0.7f, -0.5f, 1.0f), glm::vec3(-0.7f, -1.0f, 1.0f), glm::vec3(-0.2f, -0.5f, 1.0f)));
	std::shared_ptr<Primitive> tri8(new Triangle(new LambertianDiffuse(glm::vec3(0.6f, 0.6f, 0.6f), 1.0f),
		glm::vec3(-0.2f, -0.5f, 1.0f), glm::vec3(-0.7f, -1.0f, 1.0f), glm::vec3(-0.2f, -1.0f, 1.0f)));

	std::shared_ptr<Primitive> tri9(new Triangle(new LambertianDiffuse(glm::vec3(0.6f, 0.6f, 0.6f), 1.0f),
		glm::vec3(-0.7f, -0.5f, 0.5f), glm::vec3(-0.7f, -0.5f, 1.0f), glm::vec3(-0.2f, -0.5f, 0.5f)));
	std::shared_ptr<Primitive> tri10(new Triangle(new LambertianDiffuse(glm::vec3(0.6f, 0.6f, 0.6f), 1.0f),
		glm::vec3(-0.2f, -0.5f, 1.0f), glm::vec3(-0.2f, -0.5f, 0.5f), glm::vec3(-0.7f, -0.5f, 1.0f)));

	std::shared_ptr<Primitive> sphere(new Plane(glm::vec3(0, 0, 0), new LambertianDiffuse(glm::vec3(0.8f, 0.8f, 0.8f), 1.0f), glm::vec3(0, 0, 1)));
	std::shared_ptr<Primitive> sphere1(new Plane(glm::vec3(-1, 0, 0), new LambertianDiffuse(glm::vec3(0.8f, 0, 0), 1.0f), glm::vec3(1, 0, 0)));
	std::shared_ptr<Primitive> sphere2(new Plane(glm::vec3(1, 0, 0), new LambertianDiffuse(glm::vec3(0, 0.8f, 0), 1.0f), glm::vec3(-1, 0, 0)));
	std::shared_ptr<Primitive> sphere3(new Plane(glm::vec3(0, -1, 0), new LambertianDiffuse(glm::vec3(0.8f, 0.8f, 0.8f), 1.0f), glm::vec3(0, 1, 0)));
	std::shared_ptr<Primitive> sphere4(new Plane(glm::vec3(0, 1, 0), new LambertianDiffuse(glm::vec3(0.8f, 0.8f, 0.8f), 1.0f), glm::vec3(0, -1, 0)));
	std::shared_ptr<Primitive> sphere5(new Sphere(glm::vec3(0.4f, -0.8f, 0.7f), new LambertianDiffuse(glm::vec3(0.4f, 0.4f, 0.4f), 1.0f), 0.2f));
	std::shared_ptr<Primitive> sphere6(new Sphere(glm::vec3(-0.3f, -0.6f, 1.6f), new LambertianDiffuse(glm::vec3(0.6f, 0.6f, 0.6f), 1.0f), 0.4f));
	std::shared_ptr<Primitive> light(new Rectangle(glm::vec3(0, 0.999999f, 1.1f), new Emission(glm::vec3(1, 1, 1), 400.0f),
		glm::vec3(1, 0, 0), glm::vec3(0, 0, 1), 0.1f, 0.1f));
	std::vector<std::shared_ptr<Primitive>> objects;
	std::vector<std::shared_ptr<Primitive>> lights;

	objects.push_back(tri1);
	objects.push_back(tri2);
	objects.push_back(tri3);
	objects.push_back(tri4);
	objects.push_back(tri5);
	objects.push_back(tri6);
	objects.push_back(tri7);
	objects.push_back(tri8);
	objects.push_back(tri9);
	objects.push_back(tri10);

	objects.push_back(sphere);
	objects.push_back(sphere1);
	objects.push_back(sphere2);
	objects.push_back(sphere3);
	objects.push_back(sphere4);
	objects.push_back(sphere5);
	//objects.push_back(sphere6);
	objects.push_back(light);
	lights.push_back(light);


	Renderer renderer(objects, lights, std::unique_ptr<Camera>(
		new Camera(glm::vec3(0, 0, 2.5f), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0),
			1.5f, 0.004f, width, height)));

	if (!glfwInit()) {
		std::cerr << "Failed to initialize GLFW" << std::endl;
		exit(EXIT_FAILURE);
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwSetErrorCallback(error_callback);

	GLFWwindow *window = glfwCreateWindow(width, height, "glfw test", NULL, NULL);
	if (!window) {
		glfwTerminate();
		std::cerr << "Failed to create window" << std::endl;
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		std::cerr << "Failed to initialize GLAD" << std::endl;
		exit(EXIT_FAILURE);
	}

	float quad[] = {
		-1.0f, 1.0f, 0.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f, 0.0f
	};

	unsigned char indicies[] = {
		0, 1, 3,
		1, 2, 3
	};

	GLuint vao;
	GLuint vbo;
	GLuint ebo;
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);

	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indicies), indicies, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void *)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void *)(2 * sizeof(float)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	renderer.render();
	Texture2D texture(width, height, renderer.data());
	texture.bind();
	texture.setIntParam(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	texture.setIntParam(GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glActiveTexture(GL_TEXTURE0);
	texture.bind();

	Shader shader;
	try {
		shader.compile("/home/frojdholm/prog/path2/src/shader.vert", "/home/frojdholm/prog/path2/src/shader.frag");
	} catch (std::runtime_error e) {
		std::cerr << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}
	shader.use();
	shader.setInt(shader.getUniformLocation("tex"), 0);

	glBindVertexArray(vao);
	unsigned int count = 0;
	while (!glfwWindowShouldClose(window)) {
		int w, h;
		glfwGetFramebufferSize(window, &w, &h);
		glViewport(0, 0, w, h);
		glClearColor(0.1f, 0.5f, 0.8f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		renderer.render();
		texture.subImage(renderer.data());
		std::cout << "SPP: " << ++count << "\n";

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, 0);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	std::cout << std::endl;

	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ebo);

	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}
