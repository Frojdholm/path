#include "Path/Ray.h"

Ray::Ray(glm::vec3 position, glm::vec3 direction, int depth, glm::vec3 collision) : _position(position), _direction(direction), _depth(depth), _collision(collision) {}

void Ray::setCollision(glm::vec3 collision) {
    _collision = collision;
}

glm::vec3 Ray::position() const {
    return _position;
}

glm::vec3 Ray::direction() const {
    return _direction;
}

int Ray::depth() const {
    return _depth;
}

glm::vec3 Ray::collision() const {
    return _collision;
}
