#include "Path/Mesh.h"

Mesh::Mesh() {}

void Mesh::setModelMatrix(glm::mat4 matrix) {
    model = matrix;
}

void Mesh::addTriangle(Triangle tri) {
    triangles.push_back(tri);
}
