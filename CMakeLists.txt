cmake_minimum_required(VERSION 3.2)
project(Path VERSION 0.1)

set(CMAKE_BUILD_TYPE Debug)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp -Wall -Wextra")

set(PROJECT_SRCS "src/main.cpp" "src/Ray.cpp" "src/primitives.cpp" "src/render.cpp" "src/graphicutils.cpp" "src/materials.cpp")
include_directories("src/include")

set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)

add_subdirectory("external/glfw-3.2.1")
include_directories("external/glfw-3.2.1/include")

#add_subdirectory("external/glm")
include_directories("external/glm")

set(GLAD "external/glad/src/glad.c")
include_directories("external/glad/include")

find_package(OpenGL REQUIRED)
if(OPENGL_FOUND)
    include_directories(SYSTEM ${OPENGL_INCLUDE_DIR})
    set(PROJECT_LIBRARIES ${PROJECT_LIBRARIES} ${OPENGL_LIBRARIES})
endif(OPENGL_FOUND)

add_executable(${PROJECT_NAME} ${PROJECT_SRCS} ${GLAD})

target_link_libraries(${PROJECT_NAME} glfw ${GLFW_LIBRARIES})

install(TARGETS ${PROJECT_NAME} DESTINATION bin)
